# Create Ranking

Simple Rest API Create Ranking Sekolah

## Build

Create Schema Sekolah   
Run `Mvn Spring-boot:run".   

## EndPoints

1. Sekolah/AddRanking
2. Sekolah/Ranking/{filter}

## Result Test

Please see ResultTest.docx
