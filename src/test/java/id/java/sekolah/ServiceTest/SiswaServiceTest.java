package id.java.sekolah.ServiceTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.java.sekolah.model.request.CreateSiswaRequest;
import id.java.sekolah.repository.SiswaRepository;
import id.java.sekolah.service.SiswaService;

@SpringBootTest
@EnabledOnOs({OS.WINDOWS})
class SiswaServiceTest {

    @Autowired
    SiswaService siswaService;

    @Autowired
    SiswaRepository siswaRepository;
    
    @Test
    void Add(){
        CreateSiswaRequest req = new CreateSiswaRequest();
        req.setNamaSiswa("Inu");
        req.setKelas("100");
        req.setJenisKelamin("waria");

        siswaService.create(req);

        Assertions.assertNotNull(siswaRepository.findByNamaSiswa(req.getNamaSiswa()));

    }
    
}
