package id.java.sekolah.ServiceTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import id.java.sekolah.model.request.CreateMapelRequest;
import id.java.sekolah.repository.MataPelajaranRepository;
import id.java.sekolah.service.MapelService;

@SpringBootTest
@EnabledOnOs({OS.WINDOWS})
class MapelServiceTest {

    @Autowired
    MapelService mapelService;

    @Autowired
    MataPelajaranRepository mataPelajaranRepository;

    @Test
    void add(){
        CreateMapelRequest req = new CreateMapelRequest();
        req.setNamaMapel("Biologi");

        mapelService.create(req);
        Assertions.assertNotNull(mataPelajaranRepository.getByNamaMapel(req.getNamaMapel()));

    }
    
}
