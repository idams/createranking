package id.java.sekolah.service;

import java.util.List;

import id.java.sekolah.model.db.MataPelajaran;
import id.java.sekolah.model.request.CreateMapelRequest;
import id.java.sekolah.model.request.UpdateMapelRequest;

public interface MapelService {

    List<MataPelajaran> getAllMapel();
    void create(CreateMapelRequest request);
    void update(int id, UpdateMapelRequest request);
    MataPelajaran findById(int id);
    void delete(int id);
    
    
}
