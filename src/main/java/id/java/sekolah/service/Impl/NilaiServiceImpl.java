package id.java.sekolah.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import id.java.sekolah.Config.AppProperties;
import id.java.sekolah.model.db.MataPelajaran;
import id.java.sekolah.model.db.Nilai;
import id.java.sekolah.model.db.Siswa;
import id.java.sekolah.model.request.CreateNilaiRequest;
import id.java.sekolah.model.request.UpdateNilaiRequest;
import id.java.sekolah.repository.NilaiRepository;
import id.java.sekolah.service.MapelService;
import id.java.sekolah.service.NilaiService;
import id.java.sekolah.service.SiswaService;

@Service
public class NilaiServiceImpl implements NilaiService{

    @Autowired
    NilaiRepository nilaiRepository;

    @Autowired
    AppProperties appProperties;

    @Autowired
    SiswaService siswaService;

    @Autowired
    MapelService mapelService;

    @Override
    public List<Nilai> getAllNilai(){
        return nilaiRepository.findAll();
    }

    @Override
    public List<Nilai> getDataRanking(){
        return nilaiRepository.getDataRanking();
    }

    @Override
    public void create(CreateNilaiRequest request) {
    //cek NIS
    Siswa siswa = siswaService.findById(request.getNIS());
    //cek kodemapel
    MataPelajaran mapel = mapelService.findById(request.getKodeMapel());
    //cek kodemapel
    Nilai nilai = new Nilai();
    nilai.setNIS(siswa);
    nilai.setKodeMapel(mapel);
    nilai.setNilai(request.getNilai());
    nilai.setSemester(request.getSemester());
    nilai.setKodeNilai(getKodeNilai(request.getNilai()));
    nilaiRepository.save(nilai);
    }

    @Override
    public void update(int id, UpdateNilaiRequest request) {
        Nilai nilai = findById(id);
        nilai.setNilai(request.getNilai());
        nilai.setKodeNilai(getKodeNilai(request.getNilai()));
        nilai.setSemester(request.getSemester());
        nilaiRepository.save(nilai);
    }


    @Override
    public Nilai findById(int id) {
        return nilaiRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void delete(int id) {
        findById(id);
        nilaiRepository.deleteById(id);
    }

    public String getKodeNilai(Float Nilai){
    String kodeNilai = "";
        if(Nilai<60){
            kodeNilai = "E";
        }else if(Nilai>=60 && Nilai<70){
            kodeNilai = "D";
        }else if(Nilai>=70 && Nilai<80){
            kodeNilai = "C";
        }else if(Nilai>=80 && Nilai<90){
            kodeNilai = "B";
        }else if(Nilai>=90 && Nilai<100){
            kodeNilai = "A";
        }
    return kodeNilai;
    }
    
}
