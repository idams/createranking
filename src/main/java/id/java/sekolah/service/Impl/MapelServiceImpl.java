package id.java.sekolah.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import id.java.sekolah.model.db.MataPelajaran;
import id.java.sekolah.model.request.CreateMapelRequest;
import id.java.sekolah.model.request.UpdateMapelRequest;
import id.java.sekolah.repository.MataPelajaranRepository;
import id.java.sekolah.service.MapelService;

@Service
public class MapelServiceImpl implements MapelService{

    @Autowired
    MataPelajaranRepository mataPelajaranRepository;

    @Override
    public List<MataPelajaran> getAllMapel(){
        return mataPelajaranRepository.findAll();
    }

    @Override
    public void create(CreateMapelRequest request) {
        MataPelajaran mapel = new MataPelajaran();
        mapel.setNamaMapel(request.getNamaMapel());
        mataPelajaranRepository.save(mapel);
    }

    @Override
    public void update(int id, UpdateMapelRequest request) {
        MataPelajaran mapel = findById(id);
        mapel.setNamaMapel(request.getNamaMapel());
        mataPelajaranRepository.save(mapel);
    }

    @Override
    public MataPelajaran findById(int id) {
        return mataPelajaranRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void delete(int id) {
        findById(id);
        mataPelajaranRepository.deleteById(id);
    }
}
