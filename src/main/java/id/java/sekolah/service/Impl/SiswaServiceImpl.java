package id.java.sekolah.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import id.java.sekolah.model.db.Siswa;
import id.java.sekolah.model.request.CreateSiswaRequest;
import id.java.sekolah.model.request.UpdateSiswaRequest;
import id.java.sekolah.repository.SiswaRepository;
import id.java.sekolah.service.SiswaService;

@Service
public class SiswaServiceImpl implements SiswaService{

    @Autowired
    SiswaRepository siswaRepository;

    @Override
    public List<Siswa> getAllSiswa(){
        return siswaRepository.findAll();
    }

    @Override
    public void create(CreateSiswaRequest request) {
        Siswa siswa = new Siswa();
        siswa.setNamaSiswa(request.getNamaSiswa());
        siswa.setKelas(request.getKelas());
        siswa.setJenisKelamin(request.getJenisKelamin());
        siswaRepository.save(siswa);
    }

    @Override
    public void update(int id, UpdateSiswaRequest request) {
        Siswa siswa = findById(id);
        siswa.setKelas(request.getKelas());
        siswaRepository.save(siswa);
    }

    @Override
    public Siswa findById(int id) {
        return siswaRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void delete(int id) {
        findById(id);
        siswaRepository.deleteById(id);
    }
    
}
