package id.java.sekolah.service;

import java.util.List;

import id.java.sekolah.model.db.Nilai;
import id.java.sekolah.model.request.CreateNilaiRequest;
import id.java.sekolah.model.request.UpdateNilaiRequest;

public interface NilaiService {

    void create(CreateNilaiRequest request);
    List<Nilai> getAllNilai();
    Nilai findById(int id);
    void delete(int id);
    void update(int id, UpdateNilaiRequest request);
    List<Nilai> getDataRanking();
    
}
