package id.java.sekolah.service;

import java.util.List;

import id.java.sekolah.model.db.Siswa;
import id.java.sekolah.model.request.CreateSiswaRequest;
import id.java.sekolah.model.request.UpdateSiswaRequest;

public interface SiswaService {

    List<Siswa> getAllSiswa();
    void create(CreateSiswaRequest request);
    void update(int id, UpdateSiswaRequest request);
    Siswa findById(int id);
    void delete(int id);
    
}
