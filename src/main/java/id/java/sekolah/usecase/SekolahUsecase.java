package id.java.sekolah.usecase;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.java.sekolah.Config.AppProperties;
import id.java.sekolah.model.db.Nilai;
import id.java.sekolah.model.db.Ranking;
import id.java.sekolah.repository.RankingRepository;
import id.java.sekolah.service.NilaiService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SekolahUsecase {

    @Autowired
    NilaiService nilaiService;

    @Autowired
    RankingRepository rankingRepository;

    @Autowired
    AppProperties appProperties;

    public List<Ranking> getDataRanking(String filter){
        if(filter.equalsIgnoreCase(appProperties.getKelas())){
            return rankingRepository.getKelasRanking();
        }else{
            return rankingRepository.getSemesterRanking();
        }
    }


    public List<Ranking> postDataRanking(){

        Ranking ranking = new Ranking();
        List<Ranking> listRanking = new ArrayList<>();

        try {
        List<Nilai> nilai = nilaiService.getDataRanking();
        for(int i=0; i<nilai.size(); i++){
            ranking.setRanking(i+1);
            ranking.setSemester(nilai.get(i).getSemester());
            ranking.setNIS(nilai.get(i).getNIS());
            listRanking.add(ranking);
            rankingRepository.save(ranking);
        }
            
        } catch (Exception e) {
            log.info(e.getMessage());
        }

        return listRanking;
    }
    
}
