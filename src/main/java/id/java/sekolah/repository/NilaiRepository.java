package id.java.sekolah.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.java.sekolah.model.db.Nilai;

public interface NilaiRepository extends JpaRepository<Nilai, Integer>{

    @Query(value= "select n.id, n.kode_mapel_kode_mapel, n.kode_nilai, n.nilai, n.nis_nis, n.semester from nilai as n join siswa s on s.nis = n.nis_nis order by n.nilai desc", nativeQuery = true)
    List<Nilai> getDataRanking();
}
