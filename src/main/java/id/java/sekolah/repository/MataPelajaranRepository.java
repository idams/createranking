package id.java.sekolah.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.java.sekolah.model.db.MataPelajaran;

public interface MataPelajaranRepository extends JpaRepository<MataPelajaran, Integer>{

    @Query(value="select * from mata_pelajaran where nama_mapel = :nama", nativeQuery = true)
    List<MataPelajaran> getByNamaMapel(@Param("nama") String nama);
    
}
