package id.java.sekolah.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.java.sekolah.model.db.Siswa;


public interface SiswaRepository extends JpaRepository<Siswa, Integer>{

    @Query(value="select * from Siswa where nama_siswa = :nama", nativeQuery = true)
    List<Siswa> findByNamaSiswa(@Param("nama")String namaSiswa);
}
