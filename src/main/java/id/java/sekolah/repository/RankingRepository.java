package id.java.sekolah.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.java.sekolah.model.db.Ranking;

public interface RankingRepository extends JpaRepository<Ranking, Integer>{

    @Query(value= "select * from Ranking group by kelas", nativeQuery = true)
    List<Ranking> getKelasRanking();

    @Query(value= "select * from Ranking group by semester", nativeQuery = true)
    List<Ranking> getSemesterRanking();
    
}
