package id.java.sekolah.delivery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.java.sekolah.model.db.MataPelajaran;
import id.java.sekolah.model.request.CreateMapelRequest;
import id.java.sekolah.model.request.UpdateMapelRequest;
import id.java.sekolah.service.MapelService;

@RestController
@RequestMapping("Mapel")
public class MapelController {

    
    @Autowired
    MapelService mapelService;

        
    // C
    @PostMapping(value = "/Create")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody CreateMapelRequest request) {
        mapelService.create(request);
    }
    

    // R

    @GetMapping(value = "/List")
    public List<MataPelajaran> findAllMapel() {
        return mapelService.getAllMapel();
    }

        //U

    @PutMapping(value = "/Update/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable int id, @RequestBody UpdateMapelRequest request) {
        mapelService.update(id, request);
    }

    // D

    @DeleteMapping(value = "/Delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        mapelService.delete(id);
    }   
 
    
}
