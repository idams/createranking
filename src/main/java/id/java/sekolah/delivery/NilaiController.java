package id.java.sekolah.delivery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.java.sekolah.model.db.Nilai;
import id.java.sekolah.model.request.CreateNilaiRequest;
import id.java.sekolah.model.request.UpdateNilaiRequest;
import id.java.sekolah.service.NilaiService;

@RestController
@RequestMapping("Nilai")
public class NilaiController {

    @Autowired
    NilaiService nilaiService;


    //C
    @PostMapping(value = "/Create")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody CreateNilaiRequest request) {
        nilaiService.create(request);
    }

    // R

    @GetMapping(value = "/List")
    public List<Nilai> findAllNilai() {
        return nilaiService.getAllNilai();
    }


    //U

    @PutMapping(value = "/Update/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable int id, @RequestBody UpdateNilaiRequest request) {
        nilaiService.update(id, request);
    }

    // D

    @DeleteMapping(value = "/Delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        nilaiService.delete(id);
    }   


    
}
