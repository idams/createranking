package id.java.sekolah.delivery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.java.sekolah.model.db.Siswa;
import id.java.sekolah.model.request.CreateSiswaRequest;
import id.java.sekolah.model.request.UpdateSiswaRequest;
import id.java.sekolah.service.SiswaService;

@RestController
@RequestMapping("Siswa")
public class SiswaController {

    @Autowired
    SiswaService siswaService;

    // C
    @PostMapping(value = "/Create")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody CreateSiswaRequest request) {
        siswaService.create(request);
    }

   // R

    @GetMapping(value = "/List")
    public List<Siswa> findAllSiswa() {
        return siswaService.getAllSiswa();
    }

    //U

    @PutMapping(value = "/Update/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable int id, @RequestBody UpdateSiswaRequest request) {
        siswaService.update(id, request);
    }

    // D

    @DeleteMapping(value = "/Delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        siswaService.delete(id);
    }   
    
}
