package id.java.sekolah.delivery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.java.sekolah.model.db.Ranking;
import id.java.sekolah.usecase.SekolahUsecase;

@RestController
@RequestMapping("Sekolah")
public class SekolahController {
    

    @Autowired
    SekolahUsecase sekolahUsecase;

    // get NIS dan semester & create data Ranking

    @GetMapping(value = "/Ranking/{filter}")
    public List<Ranking> getListRanking(@PathVariable String filter) {
        return sekolahUsecase.getDataRanking(filter);
    }

    @PostMapping(value = "/AddRanking")
    public List<Ranking> postListRanking() {
        return sekolahUsecase.postDataRanking();
    }

}
