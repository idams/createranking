package id.java.sekolah.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UpdateNilaiRequest {

    @JsonProperty("Semester")
    private Integer Semester;
    @JsonProperty("Nilai")
    private Float Nilai;
    
}
