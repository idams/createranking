package id.java.sekolah.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateSiswaRequest {

    @JsonProperty("Nama Siswa")
    private String NamaSiswa;
    @JsonProperty("Kelas")
    private String Kelas;
    @JsonProperty("Jenis Kelamin")
    private String JenisKelamin;
        
}
