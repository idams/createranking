package id.java.sekolah.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UpdateMapelRequest {

    @JsonProperty("Nama Mapel")
    private String NamaMapel;
    
}
