package id.java.sekolah.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UpdateSiswaRequest {

    @JsonProperty("Kelas")
    private String Kelas;

}


