package id.java.sekolah.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CreateNilaiRequest {

    @JsonProperty("NIS")
    private Integer NIS;
    @JsonProperty("Kode Mapel")
    private Integer kodeMapel;
    @JsonProperty("Semester")
    private Integer Semester;
    @JsonProperty("Nilai")
    private Float Nilai;    
    
}
