package id.java.sekolah.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CreateMapelRequest {

    @JsonProperty("Nama Mapel")
    private String NamaMapel;
        
}
