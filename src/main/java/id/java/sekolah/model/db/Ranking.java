package id.java.sekolah.model.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Table
@Entity
public class Ranking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JoinColumn
    @ManyToOne
    private Siswa NIS;
    @Column
    private Integer Ranking;
    @Column
    private Integer Semester;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Siswa getNIS() {
        return NIS;
    }
    public void setNIS(Siswa nIS) {
        NIS = nIS;
    }
    public Integer getRanking() {
        return Ranking;
    }
    public void setRanking(Integer ranking) {
        Ranking = ranking;
    }
    public Integer getSemester() {
        return Semester;
    }
    public void setSemester(Integer semester) {
        Semester = semester;
    }
    

}
