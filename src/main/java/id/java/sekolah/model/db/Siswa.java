package id.java.sekolah.model.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table
@Entity
public class Siswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer NIS;
    @Column
    private String NamaSiswa;
    @Column
    private String Kelas;
    @Column
    private String JenisKelamin;

    
    public Integer getNIS() {
        return NIS;
    }
    public void setNIS(Integer nIS) {
        NIS = nIS;
    }
    public String getNamaSiswa() {
        return NamaSiswa;
    }
    public void setNamaSiswa(String namaSiswa) {
        NamaSiswa = namaSiswa;
    }
    public String getKelas() {
        return Kelas;
    }
    public void setKelas(String kelas) {
        Kelas = kelas;
    }
    public String getJenisKelamin() {
        return JenisKelamin;
    }
    public void setJenisKelamin(String jenisKelamin) {
        JenisKelamin = jenisKelamin;
    }
    
}
