package id.java.sekolah.model.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Table
@Entity
public class Nilai {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JoinColumn
    @ManyToOne
    private Siswa NIS;
    @JoinColumn
    @ManyToOne
    private MataPelajaran kodeMapel;
    @Column
    private Integer Semester;
    @Column
    private Float Nilai;
    @Column
    private String KodeNilai;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Siswa getNIS() {
        return NIS;
    }
    public void setNIS(Siswa nIS) {
        NIS = nIS;
    }
    public MataPelajaran getKodeMapel() {
        return kodeMapel;
    }
    public void setKodeMapel(MataPelajaran kodeMapel) {
        this.kodeMapel = kodeMapel;
    }
    public Integer getSemester() {
        return Semester;
    }
    public void setSemester(Integer semester) {
        Semester = semester;
    }
    public Float getNilai() {
        return Nilai;
    }
    public void setNilai(Float nilai) {
        Nilai = nilai;
    }
    public String getKodeNilai() {
        return KodeNilai;
    }
    public void setKodeNilai(String kodeNilai) {
        KodeNilai = kodeNilai;
    }

    
}
