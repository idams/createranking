package id.java.sekolah.model.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table
@Entity
public class MataPelajaran {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer KodeMapel;
    @Column
    private String NamaMapel;
    
    public String getNamaMapel() {
        return NamaMapel;
    }
    public void setNamaMapel(String namaMapel) {
        NamaMapel = namaMapel;
    }
    public Integer getKodeMapel() {
        return KodeMapel;
    }
    public void setKodeMapel(Integer kodeMapel) {
        KodeMapel = kodeMapel;
    }

    
}
